import os

import falcon


class QuoteResource:

    def on_get(self, req, resp):
        """
        Returns a quote and it's author. Depending on the locale, that is available
        in app's environment, a different quote is choosen.
        """
        local = os.getenv("locale", default="greek")
        if local == "greek":
            quote = {
                "quote": "Από τις διαφορές γεννιέται η πιο όμορφη αρμονία.",
                "author": "Ηράκλειτος",
            }
        else:
            quote = {
                "quote": "The greatest wealth is to live content with little.",
                "author": "Plato",
            }

        resp.media = quote
        resp.status = falcon.HTTP_200


app = falcon.API()

quote = QuoteResource()

app.add_route("/quote", quote)
